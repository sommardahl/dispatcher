using System.Collections;

namespace AcklenAvenue.Dispatch
{
    public interface IHandlerMatcher
    {
        IEnumerable GetMatchingHandlers(object command);
    }
}