using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AcklenAvenue.Dispatch
{
    public class DefaultHandlerMatcher : IHandlerMatcher
    {
        readonly IEnumerable<IHandler> _handlers;

        public DefaultHandlerMatcher(IEnumerable<IHandler> handlers)
        {
            _handlers = handlers;
        }
        
        public IEnumerable GetMatchingHandlers(object command)
        {           
            return _handlers.Where(HasAnInterfaceThatMatchesGenericArgs(command));
        }

        static Func<object, bool> HasAnInterfaceThatMatchesGenericArgs(object command)
        {
            
            return handler =>
            {
                var interfaces = handler.GetType()
                    .GetInterfaces();
                
                return interfaces
                    .Any(
                        i => i.GenericTypeArguments.Length == 1 &&
                             i.GenericTypeArguments[0] == command.GetType());
            };
        }

    }
}