using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace AcklenAvenue.Dispatch
{
    public sealed class Dispatcher : IDispatcher
    {
        readonly IDispatcherConfig _config;
        readonly IHandlerMatcher _handlerMatcher;

        public Dispatcher(IHandlerMatcher handlerMatcher, IDispatcherConfig config = null)
        {
            _handlerMatcher = handlerMatcher;
            _config = config ?? new DefaultDispatcherConfig();
        }

        public async Task Dispatch(object commandOrEvent)
        {
            if (commandOrEvent == null) throw new ArgumentNullException("commandOrEvent");
            _config.Logger.LogInfo(this, DateTime.UtcNow,
                $"Starting Synchronously dispatching '{commandOrEvent.GetType().Name}' with dispatcher {GetType().Name}...",
                commandOrEvent);
            await HandleTheCommand(commandOrEvent).ConfigureAwait(false);
        }

        async Task HandleTheCommand(object commandOrEvent)
        {
            var matchingCommandHandlers = _handlerMatcher.GetMatchingHandlers(commandOrEvent);
            var commandHandlers = matchingCommandHandlers as IList<object> ??
                                  matchingCommandHandlers.Cast<object>().ToList();
            if (commandHandlers.Count()<_config.MinimumHandlersPerDispatch)
                throw new NotImplementedException(
                    $"No handlers could be found to handle the '{commandOrEvent.GetType().Name}' command type .");

            var actualHandlerCount = commandHandlers.Count();
            if (actualHandlerCount > _config.MaximumHandlersPerDispatch)
                throw new TooManyHandlersException(commandOrEvent, actualHandlerCount, _config.MaximumHandlersPerDispatch);

            foreach (var handler in commandHandlers) await Handle(commandOrEvent, handler);
        }

        async Task Handle(object commandOrEvent, object handler)
        {
            _config.Logger.LogInfo(handler, DateTime.UtcNow,
                $"Synchronously dispatching '{commandOrEvent.GetType().Name}' with {handler.GetType().Name}...", commandOrEvent);
            await InvokeMethod("Handle", handler, commandOrEvent).ConfigureAwait(false);
            _config.Logger.LogInfo(handler, DateTime.UtcNow,
                $"Finished synchronously dispatching '{commandOrEvent.GetType().Name}' with {handler.GetType().Name}.",
                commandOrEvent);
        }

        async Task InvokeMethod(string methodName, object invokableObject, params object[] methodArgs)
        {
            var methodArgTypes = methodArgs.Select(x => x.GetType()).ToArray();
            try
            {
                var handlerMethod = invokableObject.GetType().GetMethod(methodName, methodArgTypes);
                await ((Task) handlerMethod.Invoke(invokableObject, methodArgs)).ConfigureAwait(false);
            }
            catch (TargetInvocationException ex)
            {
                _config.Logger.LogException(invokableObject, DateTime.UtcNow, ex, methodArgs[0]);
                throw ex.InnerException;
            }
            catch (AggregateException ex)
            {
                _config.Logger.LogException(invokableObject, DateTime.UtcNow, ex, methodArgs[0]);
                throw ex.InnerException;
            }
            catch (Exception ex)
            {
                _config.Logger.LogException(invokableObject, DateTime.UtcNow, ex, methodArgs[0]);
                throw ex.GetBaseException();
            }
        }
    }
}