using System;

namespace AcklenAvenue.Dispatch
{
    public class QuietDispatcherLogger : IDispatcherLogger
    {
        public void LogInfo(object sender, DateTime timeStamp, string message, object command)
        {            
        }

        public void LogException(object sender, DateTime timeStamp, Exception exception, object command)
        {         
        }
    }
}