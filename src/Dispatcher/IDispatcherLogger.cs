using System;

namespace AcklenAvenue.Dispatch
{
    public interface IDispatcherLogger
    {
        void LogInfo(object sender, DateTime timeStamp, string message, object command);
        void LogException(object sender, DateTime timeStamp, Exception exception, object command);
    }
}