using System.Threading.Tasks;

namespace AcklenAvenue.Dispatch
{
    public interface IHandler
    {
    }

    public interface IHandler<in TCommand> : IHandler
    {
        Task Handle(TCommand command);
    }
}