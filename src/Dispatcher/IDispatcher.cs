using System.Threading.Tasks;

namespace AcklenAvenue.Dispatch
{
   
    public interface IDispatcher
    {
        Task Dispatch(object commandOrEvent);
    }
}    