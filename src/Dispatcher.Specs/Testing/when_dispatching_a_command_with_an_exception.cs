﻿using System;
using System.Collections.Generic;
using AcklenAvenue.Commands.Specs.Stubs;
using AcklenAvenue.Dispatch;
using Machine.Specifications;

namespace AcklenAvenue.Commands.Specs.Testing
{
    public class when_dispatching_a_command_where_the_handler_has_an_exception
    {
        static Dispatcher _dispatcher;
        static readonly TestCommand TestCommand = new TestCommand();
        static Exception _exception;
        static Exception _exceptionToThrow;

        Establish context =
            () =>
            {
                _exceptionToThrow = new Exception("Test");
                var commandHandlers = new List<IHandler>
                {
                    new TestHandlerWithException(_exceptionToThrow)
                };

                _dispatcher = new Dispatcher(new DefaultHandlerMatcher(commandHandlers));
            };

        Because of =
            () => _exception = Catch.Exception(() => _dispatcher.Dispatch(TestCommand).Await());

        It should_bubble_up_the_exception =
            () => _exception.ShouldEqual(_exceptionToThrow);
    }
}