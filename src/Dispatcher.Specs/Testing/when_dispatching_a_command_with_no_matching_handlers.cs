﻿using System;
using System.Collections.Generic;
using AcklenAvenue.Commands.Specs.Stubs;
using AcklenAvenue.Dispatch;
using Machine.Specifications;

namespace AcklenAvenue.Commands.Specs.Testing
{
    public class when_dispatching_a_command_with_no_matching_handlers
    {
        static Dispatcher _dispatcher;
        static readonly TestCommand TestCommand = new TestCommand();
        static Exception _exception;

        Establish context =
            () =>
            {
                var commandHandlers = new List<IHandler>
                                      {
                                          new AnotherTestHandler()
                                      };
                _dispatcher = new Dispatcher(new DefaultHandlerMatcher(commandHandlers));
            };

        Because of =
            () => _exception = Catch.Exception(() => _dispatcher.Dispatch(TestCommand).Await());

        It should_throw_a_not_implemented_exception =
            () => _exception.ShouldBeOfExactType<NotImplementedException>();
    }
}