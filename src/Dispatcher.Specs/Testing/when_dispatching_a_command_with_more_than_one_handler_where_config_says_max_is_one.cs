using System;
using System.Collections.Generic;
using AcklenAvenue.Commands.Specs.Stubs;
using AcklenAvenue.Dispatch;
using Machine.Specifications;

namespace AcklenAvenue.Commands.Specs.Testing
{
    public class when_dispatching_a_command_with_more_than_one_handler_where_config_says_max_is_one
    {
        static Dispatcher _dispatcher;
        static readonly TestCommand TestCommand = new TestCommand();
        static Exception _exception;
        static TooManyHandlersException _exceptionToThrow;

        Establish context =
            () =>
            {
                _exceptionToThrow = new TooManyHandlersException(TestCommand, 2, 1);

                var commandHandlers = new List<IHandler>
                {
                    new TestHandler(),
                    new TestHandler()
                };
                _dispatcher = new Dispatcher(new DefaultHandlerMatcher(commandHandlers));
            };

        Because of =
            async () =>
            {
                try
                {
                    await _dispatcher.Dispatch(new TestCommand());
                }
                catch (Exception ex)
                {
                    _exception = ex;
                }
            };

        It should_bubble_up_the_exception =
            () => _exception.Message.ShouldEqual(_exceptionToThrow.Message);
    }
}