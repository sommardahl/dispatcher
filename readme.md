Acklen Avenue's Dispatcher
======================
This is a multi-purpose extendable dispatcher for .net projects. It facilitates patterns like Command Dispatch Pattern and Event Dispatch Pattern.

Installation
============
`dotnet add AcklenAvenue.Dispatcher`


Contributing
============
Acklen Avenue welcomes contributions from the community. Please create an issue describing the problem you are solving and submit a corresponding pull request with your solution. 